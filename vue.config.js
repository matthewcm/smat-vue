module.exports = {
  // options...
  devServer: {
    proxy: 'http://localhost:80/'
  },
  publicPath:
    process.env.CI_ENVIRONMENT_NAME === 'staging'
      ? '/frontends/' + process.env.CI_PROJECT_NAME + '/'
      : '/'
}
