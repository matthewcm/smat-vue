import moment from 'moment'
const { TWITTER, REDDIT, FOURCHAN, EIGHTKUN, PARLER } = require('@/constants/sites')

export default {
  namespaced: true,
  state: {
    // TODO write tests about what the required fields are
    settings: {
      searchTerm: 'election',
      startDate: moment().subtract(1, 'month').format('YYYY-MM-DD'),
      endDate: moment().format('YYYY-MM-DD'),
      websites: [{
        name: TWITTER,
        label: 'Twitter',
        active: true
      },
      {
        name: REDDIT,
        label: 'Reddit',
        active: false
      },
      // {
      //   name: GAB,
      //   label: 'Gab',
      //   active: false
      // },
      {
        name: PARLER,
        label: 'Parler',
        active: false
      },
      {
        name: FOURCHAN,
        label: '4chan',
        active: false
      },
      {
        name: EIGHTKUN,
        label: '8kun',
        active: false
      }
      ],
      aggRedditBy: 'author',
      numberOf: 10,
      interval: 'day',
      limit: 1000,
      changepoint: false
    },
    doSearch: {
      page: '', // e.g. "hashtags"
      count: 0 // kind of like a clock. when it increments, do another search
    },
    history: {
      doneSearchBefore: false,
      lastUsedTool: 'timeline'
    }
  },
  getters: {
    searchTerm (state) {
      return state.settings.searchTerm
    },
    startDate (state) {
      return state.settings.startDate
    },
    endDate (state) {
      return state.settings.endDate
    },
    websites (state) {
      return state.settings.websites
    },
    aggRedditBy (state) {
      return state.settings.aggRedditBy
    },
    numberOf (state) {
      return state.settings.numberOf
    },
    interval (state) {
      return state.settings.interval
    },
    limit (state) {
      return state.settings.limit
    },
    changepoint (state) {
      return state.settings.changepoint
    }
  },
  mutations: {
    search (state, val) {
      state.doSearch.page = val
      state.doSearch.count++
    },
    setSearchTerm (state, val) {
      state.settings.searchTerm = val
    },
    setStartDate (state, val) {
      state.settings.startDate = val
    },
    setEndDate (state, val) {
      state.settings.endDate = val
    },
    setWebsiteActive (state, { i, val }) {
      state.settings.websites[i].active = val
    },
    setAggRedditBy (state, val) {
      state.settings.aggRedditBy = val
    },
    setNumberOf (state, val) {
      state.settings.numberOf = Number(val)
    },
    setInterval (state, val) {
      state.settings.interval = val
    },
    setLimit (state, val) {
      state.settings.limit = Number(val)
    },
    setChangepoint (state, val) {
      state.settings.changepoint = val
    },
    // todo: remove?
    setDoneSearchBefore (state) {
      state.history.doneSearchBefore = true
    },
    setLastUsedTool (state, val) {
      state.history.lastUsedTool = val
    }
  },
  actions: {
    loadSettings ({ state, commit }, val) {
      // WARNING this is dangerous!!
      // require guarentees for other getters/ mutations that state.settings is a particular shape
      state.settings = val
      // some components require the variables to be numbers so we put it
      // through the mutations which do that for us
      commit('setNumberOf', val.numberOf)
      commit('setLimit', val.limit)
      // it's a string in the url but we want a boolean
      commit('setChangepoint', val.changepoint === 'true')
    }
  }
}
