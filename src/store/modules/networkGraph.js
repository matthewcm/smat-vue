import * as d3 from 'd3'
import nodeId from '@/helpers/nodeId'
import gql from 'graphql-tag'
import { apolloClient as apollo } from '@/plugins/apollo'
import { MIN_NODE_SIZE, MAX_NODE_SIZE } from '@/constants/network-graph'

// TODO - tmp, rm
var RESULTS
// RESULTS = require('./data.json')

// const pileSort = require('pile-sort')

export default {
  namespaced: true,
  state: {
    width: 80,
    height: 60,
    // margin: 5,
    zoom: 1,

    isLoading: false,
    rawData: { nodes: [], edges: [] },
    selectedNodeIds: new Set(),
    searchTerm: null
  },
  getters: {
    isLoading (state) { return state.isLoading },
    dimensions ({ width, height }) { return { width, height } },
    zoom (state) { return state.zoom },

    nodeMap (state, getters) {
      if (!state.rawData.nodes.length) return {}

      const decoratedNodes = state.rawData.nodes.map(node => {
        return Object.assign({}, node, {
          inDegree: state.rawData.edges.filter(e => e.target === node.id).length
        })
      })
      // NOTE this can't include non-CSPAN edges as we don't get those from API at moment

      const size = d3.scaleLog()
        .domain([
          d3.min(decoratedNodes, node => node.inDegree || 1),
          d3.max(decoratedNodes, node => node.inDegree || 1)
        ])
        .range([MIN_NODE_SIZE, MAX_NODE_SIZE])

      return decoratedNodes
        .map(node => Object.assign(node, { size: size(node.inDegree) }))
        .reduce((map, node) => {
          map[nodeId(node)] = node

          return map
        }, {})
    },
    selectedNodeIds (state) { return state.selectedNodeIds }, // Set
    // the nodes which the selectedNodes are pointing out to
    outboundNodeIds (state, getters) {
      if (!state.selectedNodeIds.size) return new Set()

      return getters.edges
        .reduce((acc, edge) => {
          if (state.selectedNodeIds.has(edge.source)) acc.add(edge.target)

          return acc
        }, new Set())
    },
    // the nodes which are pointing in to the selectedNodes
    inboundNodeIds (state, getters) {
      if (!state.selectedNodeIds.size) return new Set()

      return getters.edges
        .reduce((acc, edge) => {
          if (state.selectedNodeIds.has(edge.target)) acc.add(edge.source)

          return acc
        }, new Set())
    },
    // TODO instead of re-ordering these, try rendering brand new nodes on top
    nodes (state, getters) {
      return Object.values(getters.nodeMap)

      // const [selected, connected, other] = pileSort(Object.values(getters.nodeMap), [
      //   node => state.selectedNodeIds.has(nodeId(node)),
      //   node => getters.connectedNodeIds.has(nodeId(node))
      // ])

      // return [...other, ...connected, ...selected]
      // make sure any selected nodes are on top (last drawn = on top)
    },
    edges (state, getters) {
      return state.rawData.edges
    },

    searchResults (state, getters) {
      if (!state.searchTerm) return []

      const regex = new RegExp(state.searchTerm, 'i')

      return getters.nodes.filter(node => {
        return regex.test(node.id)
      })
    }
  },
  mutations: {
    setIsLoading (state, data) {
      state.isLoading = data
    },
    setRawData (state, data) {
      state.rawData = data
    },
    setDimensions (state, { width, height }) {
      state.width = width
      state.height = height
      state.radius = Math.min(width, height) / 120
    },
    setZoom (state, data) {
      state.zoom = data
    },
    setSearchTerm (state, data) {
      state.searchTerm = data
    },
    setSelectedNodeIds (state, data) {
      state.selectedNodeIds = new Set(data)
    }
  },
  actions: {
    fetchInitialState ({ commit }) {
      commit('setIsLoading', true)

      if (RESULTS) {
        commit('setRawData', tidyUpGraphResponse(RESULTS.data.networkgraph))
        commit('setIsLoading', false)
        return
      }

      const t = Date.now()
      apollo.query({
        query: gql`query {
          networkgraph(name: "twitter_us2020"){
            nodes(collections: "twitter_user") {
              Key
            }

            replies: edges(collections: ["twitter_reply_to"]) {
              From
              To
              weight
            }

            mentions: edges(collections: ["twitter_mention"]) {
              From
              To
              weight
            }
          }
        }`,
        fetchPolicy: 'no-cache'
      })
        .then(results => {
          console.info(`<-- GraphQL networkgraph ${(Date.now() - t) / 1000}s`)
          commit('setRawData', tidyUpGraphResponse(results.data.networkgraph))
          commit('setIsLoading', false)
        })
        .catch(err => {
          console.error(err)
          // TODO commit error state
          commit('setIsLoading', false)
        })
    },
    setDimensions ({ commit }, dimensions) {
      commit('setDimensions', dimensions)
    },
    setZoom ({ commit }, t) {
      commit('setZoom', t)
    },
    search ({ commit }, searchTerm) {
      commit('setSearchTerm', searchTerm)
    },
    setSelectedNodeIds ({ commit }, nodeIds) {
      commit('setSelectedNodeIds', nodeIds)
      // TODO commit changes to networkDetails
    }
  }
}

function tidyUpGraphResponse ({ nodes, replies, mentions }) {
  const _nodes = nodes.map(node => ({ id: node.Key }))
  const nodeIds = new Set(_nodes.map(n => n.id))

  // TODO add nodes mentioned in edges ...

  const edges = [
    ...mentions.map(e => ({
      type: 'mention',
      source: e.From,
      target: e.To,
      weight: e.weight
    })),
    ...replies.map(e => ({
      type: 'reply',
      source: e.From,
      target: e.To,
      weight: e.weight
    }))
  ]
  // TODO From / To tidied up in the API so don't need to trim

  const edgesWithEnds = edges
    .filter(edge => (
      edge.source !== edge.target &&
      nodeIds.has(edge.source) &&
      nodeIds.has(edge.target)
    ))
    // some edges don't have end points?
    // OR some end points are deleted (as node has no coords)... so the edge becomes invalid
  if (edgesWithEnds.length !== edges.length) {
    console.error(`WARNING: ${edges.filter(e => e.source === e.target).length} of ${edges.length} edges had same source + target!`)
    console.error(`WARNING: only ${edgesWithEnds.length} of ${edges.length} edges had unique, defined endpoints`)
  }

  return {
    nodes: _nodes,
    edges: edgesWithEnds
  }
}
