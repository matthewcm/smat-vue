import gql from 'graphql-tag'
import { apolloClient as apollo } from '@/plugins/apollo'

export default {
  namespaced: true,
  state: {
    accountId: null,
    isLoading: false, // not in use ??
    store: {} // apollo could be better for this
  },

  getters: {
    accountId (state) {
      return state.accountId
    },
    accountDetails (state) {
      return state.store[state.accountId]
    }
  },

  mutations: {
    setAccountId (state, id) {
      state.accountId = id
    },
    setIsLoading (state, bool) {
      state.isLoading = bool
    },
    setShowDetails (state, bool) {
      state.showDetails = bool
    },
    updateAccountDetails (state, { accountId, data }) {
      state.store = Object.assign(
        {},
        state.store,
        {
          [accountId]: Object.assign({}, state.store[accountId], {
            id: accountId,
            name: data.name,
            description: data.description,

            outboundMentions: data.outboundMentions.sort(bigWeight),
            inboundMentions: data.inboundMentions.sort(bigWeight),
            outboundReplies: data.outboundReplies.sort(bigWeight),
            inboundReplies: data.inboundReplies.sort(bigWeight)
          })
        }
      )
    }
  },

  actions: {
    loadAccountDetails ({ commit }, { accountId }) {
      commit('setAccountId', accountId)
      commit('setIsLoading', true)

      apollo.query({
        query: gql`query ($accountId: String!) {
          account(collection: "twitter_user", key: $accountId) {
            Key
            name
            description

            outboundMentions: edges(collection: "twitter_mention", direction: "outbound") {
              To
              weight
            }
            inboundMentions: edges(collection: "twitter_mention", direction: "inbound") {
              From
              weight
            }

            outboundReplies: edges(collection: "twitter_reply_to", direction: "outbound") {
              To
              weight
            }
            inboundReplies: edges(collection: "twitter_reply_to", direction: "inbound") {
              From
              weight
            }
          }
        }`,
        variables: {
          accountId
        }
        // fetchPolicy: 'no-cache'
      })
        .then(results => {
          commit('setIsLoading', false)

          const data = results.data.account
          data.id = data.Key

          commit('updateAccountDetails', { accountId, data })
        })
        .catch(err => {
          console.error(err)
          // TODO commit error state
          commit('setIsLoading', false)
        })
    }
  }
}

function bigWeight (a, b) {
  return b.weight - a.weight
}
