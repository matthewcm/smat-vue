import searchSettings from './modules/searchSettings'
import networkGraph from './modules/networkGraph'
import networkDetails from './modules/networkDetails'

export default {
  modules: {
    searchSettings,
    networkGraph,
    networkDetails
  }
}
