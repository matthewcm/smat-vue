const PRODUCTION = 'production'

module.exports = {
  PRODUCTION,
  ENV: (
    process.env.VUE_APP_ENV_DISPLAY ||
      process.env.NODE_ENV ||
      PRODUCTION
  ),

  HOME: 'home',
  ABOUT: 'about',
  NETWORK: 'network',
  NETWORK_ACCOUNT: 'network-account',
  NETWORK_ACCOUNT_INTERACTION: 'network-account-interaction'
}
