"use strict";

module.exports = {
  TWITTER: 'twitter',
  REDDIT: 'reddit',
  GAB: 'gab',
  FOURCHAN: '4chan',
  EIGHTKUN: '8kun',
  PARLER: 'parler'
};