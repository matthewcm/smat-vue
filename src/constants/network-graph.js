export const MIN_NODE_SIZE = 3
export const MAX_NODE_SIZE = 10
export const DEFAULT_NODE_COLOR = '#999'

export const DEFAULT_EDGE_COLOR = '#555'
export const DEFAULT_EDGE_WIDTH = 0.1
