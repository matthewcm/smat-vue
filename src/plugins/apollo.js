import Vue from 'vue'
import VueApollo from 'vue-apollo'

import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

Vue.use(VueApollo)

export const apolloClient = createApolloClient()

export default new VueApollo({
  defaultClient: apolloClient
})

function createApolloClient () {
  // HTTP connection to the API
  const httpLink = createHttpLink({
    // You should use an absolute URL here
    uri: 'https://network-api-ngfrlpn57a-ue.a.run.app/graphql'
    // TODO change for production
  })

  // Cache implementation
  const cache = new InMemoryCache()

  // Create the apollo client
  return new ApolloClient({
    link: httpLink,
    cache
  })
}
