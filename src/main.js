import Vue from 'vue'

import App from './App.vue'
import router from './router'
import i18n from './plugins/i18n'
import store from './plugins/vuex'
import apolloProvider from './plugins/apollo'
import './plugins/quasar'

import { ENV, PRODUCTION } from './constants/misc'

Vue.config.productionTip = false

if (ENV !== PRODUCTION) {
  document.title = ENV.toUpperCase()
}

new Vue({
  router: router(store),
  store,
  i18n,
  apolloProvider,
  render: (h) => h(App)
}).$mount('#app')
