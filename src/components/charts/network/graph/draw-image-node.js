import { MIN_NODE_SIZE, DEFAULT_NODE_COLOR } from '@/constants/network-graph'

export default function drawImageNode (ctx, imageEl, d, opts = {}) {
  const radius = (opts.size || d.size || MIN_NODE_SIZE) - 1

  ctx.beginPath()
  ctx.arc(d.x, d.y, radius + 1, 0, 2 * Math.PI)
  ctx.fillStyle = opts.color || DEFAULT_NODE_COLOR
  ctx.fill()

  if (opts.label) {
    ctx.font = '8px Arial'
    ctx.textAlign = 'center'

    ctx.strokeStyle = '#fff'
    ctx.strokeText(d.id, d.x, d.y + (radius + 1) + 8)

    ctx.fillStyle = '#2a2d43'
    ctx.fillText(d.id, d.x, d.y + (radius + 1) + 8)
  }

  ctx.beginPath()
  ctx.moveTo(d.x + radius, d.y)
  ctx.arc(d.x, d.y, radius, 0, 2 * Math.PI)
  ctx.clip()
  ctx.drawImage(imageEl, d.x - radius, d.y - radius, radius * 2, radius * 2)
}
