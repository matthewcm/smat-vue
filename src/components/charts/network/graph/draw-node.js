import { MIN_NODE_SIZE, DEFAULT_NODE_COLOR } from '@/constants/network-graph'

export default function drawNode (ctx, d, opts = {}) {
  ctx.beginPath()
  ctx.moveTo(d.x + (opts.size || d.size || MIN_NODE_SIZE), d.y)
  ctx.arc(d.x, d.y, (opts.size || d.size || MIN_NODE_SIZE), 0, 2 * Math.PI)

  ctx.fillStyle = opts.color || DEFAULT_NODE_COLOR
  // ctx.lineWidth = 0

  ctx.fill()
  // ctx.stroke()

  if (opts.label) {
    ctx.font = '8px Arial'
    ctx.textAlign = 'center'

    ctx.strokeStyle = '#fff'
    ctx.strokeText(d.id, d.x, d.y + (opts.size || d.size || MIN_NODE_SIZE) + 8)

    ctx.fillStyle = '#2a2d43'
    ctx.fillText(d.id, d.x, d.y + (opts.size || d.size || MIN_NODE_SIZE) + 8)
  }
}
