import { DEFAULT_EDGE_WIDTH, DEFAULT_EDGE_COLOR } from '@/constants/network-graph'

export default function drawEdge (ctx, d, opts = {}) {
  ctx.beginPath()
  ctx.moveTo(d.source.x, d.source.y)
  ctx.lineTo(d.target.x, d.target.y)

  ctx.strokeStyle = opts.color || DEFAULT_EDGE_COLOR
  ctx.lineWidth = opts.width || DEFAULT_EDGE_WIDTH

  ctx.stroke()
}
