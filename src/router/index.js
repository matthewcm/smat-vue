import Vue from 'vue'
import VueRouter from 'vue-router'
import AboutView from '@/views/AboutView'
import TimelineView from '@/views/TimelineView'
import HashtagsView from '@/views/HashtagsView'
import LinksView from '@/views/LinksView'
import ActivityView from '@/views/ActivityView'

import SettingsTimeline from '@/components/sidebar/SettingsTimeline'
import SettingsHashtags from '@/components/sidebar/SettingsHashtags'
import SettingsLinks from '@/components/sidebar/SettingsLinks'
import SettingsActivity from '@/components/sidebar/SettingsActivity'

import NetworkView from '@/views/NetworkView'
import DetailsNetwork from '@/components/sidebar/DetailsNetwork'
import DetailsNetworkInteraction from '@/components/sidebar/DetailsNetworkInteraction'

const { HOME, ABOUT } = require('@/constants/misc')
const { TIMELINE, HASHTAGS, LINKS, ACTIVITY } = require('@/constants/tools')
const { NETWORK, NETWORK_ACCOUNT, NETWORK_ACCOUNT_INTERACTION } = require('@/constants/misc')

export default function router (store) {
  Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: HOME,
      redirect: () => {
        const history = store.state.searchSettings.history

        return { name: history.lastUsedTool }
      }
    },
    {
      path: '/about',
      name: ABOUT,
      components: {
        page: AboutView
      }
    },
    {
      path: '/timeline',
      name: TIMELINE,
      components: {
        page: TimelineView,
        settings: SettingsTimeline
      }
    },
    {
      path: '/hashtags',
      name: HASHTAGS,
      components: {
        page: HashtagsView,
        settings: SettingsHashtags
      }
    },
    {
      path: '/links',
      name: LINKS,
      components: {
        page: LinksView,
        settings: SettingsLinks
      }
    },
    {
      path: '/activity',
      name: ACTIVITY,
      components: {
        page: ActivityView,
        settings: SettingsActivity
      }
    },

    {
      path: '/network',
      name: NETWORK,
      components: {
        page: NetworkView
      }
    },
    {
      path: '/network/account/:accountId',
      name: NETWORK_ACCOUNT,
      components: {
        page: NetworkView,
        details: DetailsNetwork
      }
    },
    {
      path: '/network/account/:accountId/:interaction',
      name: NETWORK_ACCOUNT_INTERACTION,
      components: {
        page: NetworkView,
        details: DetailsNetworkInteraction
      }
    },
    // TODO - figure out how to do nested routes in a way which doesn't break :(
    // {
    //   path: '/network',
    //   name: NETWORK,
    //   components: {
    //     page: NetworkView
    //   },
    //   children: [
    //     {
    //       path: 'account/:accountId',
    //       name: NETWORK_ACCOUNT,
    //       components: {
    //         page: NetworkView,
    //         details: DetailsNetwork
    //       }
    //     }
    //   ]
    // }
  ]

  return new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })
}
