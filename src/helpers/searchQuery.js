const { REDDIT, TWITTER, GAB, FOURCHAN, EIGHTKUN, PARLER } = require('@/constants/sites')
const { HASHTAGS, LINKS, TIMELINE, ACTIVITY } = require('@/constants/tools')

let apiBaseUrl = 'https://smat-be.herokuapp.com'
if (process.env.VUE_APP_API) { // e.g. http://127.0.0.1:5000
  apiBaseUrl = process.env.VUE_APP_API
  console.info('VUE_APP_API:', apiBaseUrl)
}

export function queries (settings, page) {
  const sites = (page === 'hashtags')
    ? [{ label: 'Twitter', name: TWITTER, active: true }]
    : settings.websites
  // TODO replace line 12 logic and just filter out the Twitter website

  return sites
    .filter((website) => website.active)
    .map((website) => {
      let slug = ''
      let query = ''

      // TODO change this to use https://devdocs.io/node~10_lts/url Url, URLSearchParams
      // But need to check where else in the app this is being used, as might make search terms use URL encoding (good)
      // which could break any custom parsing elsewhere
      switch (page) {
        case HASHTAGS:
        case LINKS:
          slug = 'content'
          query = `?site=${website.name}&term=${settings.searchTerm}&limit=${settings.limit}&since=${settings.startDate}&until=${settings.endDate}`
          break

        case TIMELINE:
          slug = 'timeseries'
          query = `?site=${website.name}&term=${settings.searchTerm}&interval=${
            settings.interval
          }&since=${settings.startDate}&until=${settings.endDate}${
            settings.changepoint ? '&changepoint=True' : ''
          }`
          break

        case ACTIVITY:
          slug = 'activity'
          query = `?site=${website.name}&term=${settings.searchTerm}&since=${settings.startDate}&until=${settings.endDate}&agg_by=${
            chooseAggBy(website, settings)
          }`
          break

        default:
          throw new Error(`Invalid page: ${page}`)
      }

      return {
        ...website,
        url: `${apiBaseUrl}/${slug}${query}`
      }
    })
}

function chooseAggBy (website, settings) {
  let aggBy = 'author'
  if (website.name === REDDIT) { aggBy = settings.aggRedditBy }
  if (website.name === TWITTER) { aggBy = 'screen_name' }
  if (website.name === GAB) { aggBy = 'account.acct' }
  if (website.name === FOURCHAN) { aggBy = 'name' }
  if (website.name === EIGHTKUN) { aggBy = 'name' }
  if (website.name === PARLER) { aggBy = 'username' }
  return aggBy
}