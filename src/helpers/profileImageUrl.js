export default function profileImageUrl (url, id) {
  // NOTE currently the url provided is blocked as a "tracker" related link in firefox

  return `https://twitter-avatar.now.sh/${id}`
}
