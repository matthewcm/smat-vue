export default function nodeId (node) {
  return node.screenName || node.id || node.Key
}
