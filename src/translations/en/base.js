const { TIMELINE, HASHTAGS, LINKS, ACTIVITY } = require('../../constants/tools')
const { HOME, NETWORK, ABOUT } = require('../../constants/misc')

module.exports = {
  nav: {
    [HOME]: 'Home',
    [NETWORK]: '2020 Network Graph',
    [ABOUT]: 'About',
    [TIMELINE]: 'Timeline',
    [HASHTAGS]: 'Hashtags',
    [LINKS]: 'Link Counter',
    [ACTIVITY]: 'Activity'
  },
  toolPicker: {
    label: 'SMAT tool',
    changeTools: 'Change tools',
    modalTitle: 'Choose a tool to begin your analysis',
    modalSubtitle:
      'Each tool below will run a different analysis based on your input and return downloadable results'
  },
  toolDescription: {
    [TIMELINE]:
      'The timeline functionality takes a search term and makes a graph of how often it was written over time',
    [HASHTAGS]:
      'The hashtag functionality takes a search term and creates a bar graph of related hashtags',
    [LINKS]:
      'The link counter functionality takes a search term and returns a bar graph counting links in comments with that term',
    [ACTIVITY]:
      'This takes a search term and creates a bar graph of which authors or subreddits have the most activity',
    [NETWORK]:
      'See the current Network Activity on Twitter for political accounts around the 2020 election.'
  },
  setting: {
    term: 'Search term',
    termPlaceholder: 'Enter search term',
    startDate: 'Start date',
    endDate: 'End date',
    pickWebsite: 'Social media website to search',
    aggBy: {
      label: 'Aggregate Reddit by',
      author: 'Author',
      subreddit: 'Subreddit'
    },
    numberHashtags: 'Number of hashtags',
    numberUrls: 'Number of urls',
    interval: 'Interval',
    times: {
      hour: 'hour',
      day: 'day',
      week: 'week',
      month: 'month'
    },
    timely: {
      hour: 'Hourly',
      day: 'Daily',
      week: 'Weekly',
      month: 'Monthly'
    },
    limit: 'Limit',
    showChangepoint: 'Show changepoint',
    button: {
      timeline: 'See timeline results',
      hashtags: 'See hashtag detector results',
      urls: 'See URLs graph',
      activity: 'See activity'
    }
  },
  chart: {
    timelineOn: 'Timeline on ',
    hashtagsWith: 'Hashtags Occurring with ',
    linkCountOn: 'Link Count on ',
    activityOn: 'Activity on ',
    postsPer: "Posts containing '{term}' per {interval}",
    popularLinksWith: "Popular links occurring with '{term}'",
    termAggBy: "'{term}' aggregated over {aggBy}s",
    downloadPNG: 'Save as PNG'
  },
  table: {
    date: 'Date',
    usage: 'Usage',
    urls: 'URLs',
    urlCount: 'URL count',
    user: 'User',
    count: 'Count',
    hashtags: 'Hashtags',
    hashtagFreq: 'Hashtag frequency',
    viewFor: 'Table View for ',
    view: 'Table View',
    downloadCSV: 'Download CSV'
  },
  data: {
    beforeChangepoint: 'Before {site} changepoint',
    afterChangepoint: 'After {site} changepoint'
  }
}
