module.exports = {
  title: 'About SMAT',
  subtitle: 'Easy sense-making for online conversations.',
  intro: 'The Social Media Analysis Toolkit (SMAT) was designed to help facilitate activists, journalists, researchers, and other social good organizations to analyze and visualize larger trends on a variety of platforms.',
  // We provide easy to use tools that help you tell a story about something happening at a large scale online. The tools are designed to be quick and simple so that users can quickly respond to things like disinformation and hate as they occur, while also having the tools be rigorous enough to have applications in <a href="https://workshop-proceedings.icwsm.org/abstract?id=2020_18">scholarly research</a>. While we have built the framework, it is up to you and your curiosity to find what stories are waiting to be discovered.',

  value1: {
    title: 'What we provide',
    detail: 'We provide easy to use tools that help you tell a story about something happening at a large scale online.'
  },
  value2: {
    title: 'Simple & Rigorous',
    detail: 'The tools are designed to be quick and simple so that users can quickly respond to things like disinformation and hate as they occur, while also having the tools be rigorous enough to have applications in scholarly research.'
  },
  value3: {
    title: "We're open source",
    detail: 'All our tools are {openSource}, so you can see our work and modify it for your own needs.',
    openSource: 'open source'
  },

  contact: {
    title: 'Contact Us',
    detail: 'Find us on {gitHosting} or say "Hey!" at rebelliousdata[at]gmail.com or @emmibevensee on Twitter. SMAT is fiscally hosted and small or large contributions can be made through our {donation} page which help to develop new tools and data sources. Descriptions of bugs or desired features welcomed as well!'
  },

  contributors: {
    title: 'Contributors',
    detail: 'Emmi Bevensee, Max Aliapoulios, Jason Baumgartner, Harry Momand, {protozoa}, Mix Irving, Quinn Dougherty, Jeremy Blackburn, ZC, {jacob}, Zach!, and others. Special thanks to the incredible {iDrama} research lab for their support. Tools built with {pushshift}. Inspired by the {slackbot}. Made in with collaboration and support from the {ncri}. Designs by {mek}.'
  },
  partners: {
    title: 'Our Partners'
  }
}
